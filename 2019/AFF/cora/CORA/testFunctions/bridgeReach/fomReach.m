function fomReach()
% updated: 06-November-2018, MA

% set Krylov
Krylov = 1;

% set output
output = 1;

%system matrix
load fom.mat % built-in MATLAB example

%obtain dimension
dim = length(A);

%set options --------------------------------------------------------------
options.tStart = 0; %start time
options.tFinal = 1e-1; %final time
%options.tFinal = 0.2*1e-1; %final time
options.x0 = ones(dim,1); %initial state for simulation
%options.R0 = zonotope([options.x0,0.1*eye(dim)]); %initial state for reachability analysis
I = eye(dim);
options.R0=zonotope(sparse([options.x0,10*I(:,1:10)])); %initial state for reachability analysis

options.timeStep=1e-4; %time step size for reachable set computation
options.taylorTerms=4; %number of taylor terms for reachable sets
options.zonotopeOrder=10; %zonotope order
options.errorOrder=1;
options.polytopeOrder=2; %polytope order
options.reductionTechnique='girard'; %polytope order

options.plotType='frame';

%options.W = pinv(diag([0.1,0.1,0.5,10,1,1]));
%options.originContained = 0;
options.originContained = 1;
options.reductionInterval = inf;
options.advancedLinErrorComp = 1;
options.tensorOrder = 2;

if Krylov
    options.KrylovError = eps;
    options.KrylovStep = 20;
    options.saveOrder = 1;
else
    options.saveOrder = 1;
end
%--------------------------------------------------------------------------


%obtain uncertain inputs
options.uTrans = 0;
options.U = zonotope([0 0.1]);

%specify continuous dynamics-----------------------------------------------
if output
    fomDyn = linearSys('fomDynamics',A,B,[],C); %initialize fom dynamics
else
    fomDyn = linearSys('fomDynamics',A,B); %initialize fom dynamics
end
%--------------------------------------------------------------------------


%compute reachable set using zonotope bundles
%profile on
tic
if output && ~Krylov
    [Rcont,Rcont_tp,Rcont_y] = reach(fomDyn, options);
else
    Rcont = reach(fomDyn, options);
end
toc 
% profile off
% profile viewer

%simulate
stepsizeOptions = odeset('MaxStep',0.2*(options.tFinal-options.tStart));
%generate overall options
opt = odeset(stepsizeOptions);

%initialize
runs=40;
finalTime=options.tFinal;

for i=1:runs

    %set initial state, input
    if i<=30
        options.x0=randPointExtreme(options.R0); %initial state for simulation
    else
        options.x0=randPoint(options.R0); %initial state for simulation
    end

    %set input
    if i<=8
        options.u=randPointExtreme(options.U)+options.uTrans; %input for simulation
    else
        options.u=randPoint(options.U)+options.uTrans; %input for simulation
    end

    %simulate hybrid automaton
    [fomDyn,t{i},x{i}] = simulate(fomDyn,options,options.tStart,options.tFinal,options.x0,opt);
    x_proj{i} = (fomDyn.C*x{i}')';
end


% Visualization -----------------------------------------------------------
figure
hold on
i = 1;
iDim = 1;
% plot time elapse
while i<=length(Rcont)
    % get Uout 
    t1 = (i-1)*options.timeStep;
    try
        minVal = inf;
        maxVal = -inf;
        Uout = interval(project(Rcont{i},iDim));
        if infimum(Uout) < minVal
            minVal = infimum(Uout);
        end
        if supremum(Uout) > maxVal
            maxVal = supremum(Uout);
        end
        i = i + 1;
    catch
        minVal = infimum(interval(project(Rcont{i-1},iDim)));
        maxVal = supremum(interval(project(Rcont{i-1},iDim)));
    end
    t2 = (i-1)*options.timeStep;
    % generate plot areas as interval hulls
    IH1 = interval([t1; minVal], [t2; maxVal]);

    plotFilled(IH1,[1 2],[.75 .75 .75],'EdgeColor','none');
end

% plot simulation results
for i=1:(length(t))
    plot(t{i},x_proj{i},'Color',0*[1 1 1]);
end



        
