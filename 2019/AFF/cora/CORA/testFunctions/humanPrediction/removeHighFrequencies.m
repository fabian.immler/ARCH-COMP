function corrTraj = removeHighFrequencies(fullTraj)
% removeHighFrequencies - removes high frequencies of recorded data
%
% Syntax:  
%    corrTraj = removeHighFrequencies(fullTraj)
%
% Inputs:
%    fullTraj - recorded trajectory of all markers
%
% Outputs:
%    corrTraj - corrected trajectory of all markers
%
% Example: 
%
% 
% Author:       Matthias Althoff
% Written:      10-March-2019
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------


% % cut of frequency set to 10Hz
% cutOff = 10;

% filgter window
window = 10;
window = 5;

%median filter
corrTraj = medfilt1(fullTraj,window,'truncate');

% plot
for i=1:length(fullTraj(1,:))
    figure 
    hold on
    plot(fullTraj(:,i));
    plot(corrTraj(:,i));
end

%------------- END OF CODE --------------