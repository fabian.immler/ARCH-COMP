function E=reduce(E_in)
% reduce - reduces E_in to its biggest non-degenerate form
%
% Syntax:  
%    E = reduce(E_in)
% Inputs:
%    E_in - ellipsoid object
%
% Outputs:
%    E - ellipsoid object
%
% Example: 
%    E_in=ellipsoid([1 0 0; 0 0 0;0 0 3]);
%    E = reduce(E_in);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Matthias Althoff
% Written:      27-July-2016
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
if E_in.dim==length(E_in.Q)
    E = E_in;
    return;
end
[U,S,V] = svd(E_in.Q);
Q = U(:,1:E_in.dim)*S(1:E_in.dim,1:E_in.dim)*V(:,1:E_in.dim)';
E = ellipsoid(Q,E_in.q);
%------------- END OF CODE --------------